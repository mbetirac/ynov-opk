#!/bin/bash

# Update du système et installe des paquets nécéssaires
apt update 
apt -y install apache2 mariadb-server php php-common php-mysql php-gmp php-curl php-intl php-mbstring php-xmlrpc php-gd php-xml php-cli php-zip 
systemctl start mariadb apache2 
systemctl enable mariadb apache2 

# création db

mysql -u root -e "CREATE DATABASE wordpress;"
mysql -u root -e "CREATE USER 'wp_user'@'localhost' IDENTIFIED BY 'Passw0rd';"
mysql -u root -e "GRANT ALL ON wordpress.* TO 'wp_user'@'localhost' WITH GRANT OPTION;"
mysql -u root -e "FLUSH PRIVILEGES;"

# Installe wordpress

cd /tmp
wget https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
mv wordpress /var/www/html/wordpress
chown -R www-data:www-data /var/www/html/wordpress/
chmod -R 755 /var/www/html/wordpress/

# conf vhost
a2dissite 000-default.conf
a2enmod ssl
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/ssl/private/cert.key -out /etc/ssl/certs/cert.crt -subj "/C=FR/ST=ARA/L=Lyon/O=Ynov/OU=IT/CN=testopk.local/emailAddress=maxime.betirac@ynov.com"
touch /var/log/apache2/error.testopk.log /var/log/apache2/access.testopk.log
cat <<EOF >>/etc/apache2/sites-available/wordpress.conf
<VirtualHost *:80>
        ServerName testopk.local
        Redirect permanent / https://testopk.local
</VirtualHost>

<VirtualHost *:443>
        ServerName testopk.local
        DocumentRoot "/var/www/html/wordpress"
        DirectoryIndex index.php
        # Logs
        ErrorLog /var/log/apache2/error.testopk.log
        CustomLog /var/log/apache2/access.testopk.log combined
        # SSL & SÃ©curisation
        SSLCertificateFile /etc/ssl/certs/cert.crt
        SSLCertificateKeyFile /etc/ssl/private/cert.key
</VirtualHost>
EOF
a2ensite wordpress.conf
systemctl reload apache2