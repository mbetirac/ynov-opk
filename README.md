# Hacking the TP

L'objectif du TP est de déployer un wordpress **fonctionnel** sur une instance openstack

Ce repo sera simplement cloné et exécuté pour déployer un wordpress 🤓

# Before using

Juste installer git, cloner le repo et lancer le fichier requirements.sh avec root :D

# Résultats

En gros le script est fait pour une machine Debian12, et installe tout ce qu'il faut (DB comprise) pour faire tourner un wordpress

Les infos de connexion à la DB sont les suivantes:

DB Name : wordpress

DB User : wp_user

DB Pass : Passw0rd

Un certificat SSL autosigné est également mis en place pour répondre au CN 'testopk.local'

Simplement ajouter une entrée dans le fichier hosts pour profiter de cette URL

Testé tard dans la nuit mais normalement c'est plug-and-play

Une fois le script exécuté et le fichier hosts modifié, se rendre sur https://testopk.local et enjoy!